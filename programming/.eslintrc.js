module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: 'eslint:recommended',
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
      legacyDecorators: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: ['react'],
  rules: {
    'no-console': 'off',
    'react/jsx-uses-vars': [2],
    'no-mixed-spaces-and-tabs': [2, 'smart-tabs'],
    indent: ['error',2],
    'linebreak-style': ['error', 'unix'],
    quotes: ['error'],
    semi: ['error', 'always'],
  },
};

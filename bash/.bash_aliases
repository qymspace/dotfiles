#!/bin/bash
DIR=~/dotfiles/bash/aliases/
for file in $(ls ~/dotfiles/bash/aliases/); do
	[ -r "$DIR$file" ] && [ -f "$DIR$file" ] && source "$DIR$file";
done;
unset DIR
unset file;


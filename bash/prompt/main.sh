#!/bin/bash
DIR=~/dotfiles/bash/prompt

source "$DIR/base.sh"
source "$DIR/colors.sh"
source "$DIR/prompt.sh"

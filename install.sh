#!/bin/bash

#
#Install stow
sudo apt install stow -y

cd ~/
echo "Cloning into dotfiles"
git clone https://gitlab.com/qymspace/dotfiles.git 
curl -fLo ~/dotfiles/vim/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

echo "Installing plugins..."
vim +PlugInstall +qall
#symlink everything
cd ~/dotfiles

stow vim
stow i3
stow bash

".vimrc
" author Bethwel Kimutai<bethwelkim@gmail.com>
" source https://www.github.com/qymspace/dotfiles/vim/.vimrc

"enable syntax highlighting
syntax on

"AUTOCOMPLETE
filetype plugin indent on
set omnifunc=syntaxcomplete#Complete
"Set colors
colorscheme elflord

"set autoindent
set autoindent

"set case insensitive search
set ignorecase
set smartcase


" Folding {{{
"=== folding ===
set foldmethod=indent   " fold based on indent level
set foldnestmax=10      " max 10 depth
" set foldenable          " don't fold files by default on open
nnoremap <space> za
set foldlevelstart=10   " start with fold level of 1
" }}}
" Line Shortcuts {{{
nnoremap j gj
nnoremap k gk
nnoremap gV `[v`]
" }}}
" Leader Shortcuts {{{
let mapleader=","
"}}}

"Mappings
map gn :bn<cr>
map gp :bp<cr>
map gd :bd<cr>


"set a number of preference options
set relativenumber
set number
set incsearch


"vim-plug config
call plug#begin('~/.vim/plugged')
"NerdTree
Plug 'scrooloose/nerdtree'
"Prettier
Plug 'prettier/vim-prettier', {
  \ 'do': 'yarn install',
  \ 'branch': 'release/1.x',
  \ 'for': [
    \ 'javascript',
    \ 'typescript',
    \ 'css',
    \ 'less',
    \ 'scss',
    \ 'json',
    \ 'graphql',
    \ 'markdown',
    \ 'vue',
    \ 'lua',
    \ 'php',
    \ 'python',
    \ 'ruby',
    \ 'html',
    \ 'swift' ] }
"Vim javascript
Plug 'pangloss/vim-javascript', { 'for': 'javascript' }
"VimJSX
Plug 'mxw/vim-jsx',

"Emmet
Plug 'mattn/emmet-vim'
"Ale
Plug 'w0rp/ale'
"Async Run
Plug 'skywind3000/asyncrun.vim'
"CtrlP
Plug 'ctrlpvim/ctrlp.vim'
"Sorounding
Plug 'tpope/vim-surround'
"Repeat.vim
Plug 'tpope/vim-repeat'
"Snippets
Plug 'SirVer/ultisnips'
"Snippets
Plug 'honza/vim-snippets'
" ES2015 code snippets 
Plug 'epilande/vim-es2015-snippets'
" React code snippets
Plug 'epilande/vim-react-snippets'
"Sort motion
Plug 'christoomey/vim-sort-motion'
"Comment using textobject
Plug 'tpope/vim-commentary'
"Surround using motions and textobject
Plug 'tpope/vim-surround'
Plug 'Raimondi/delimitMate'

Plug 'Valloric/YouCompleteMe', { 'do': './install.py' }
"Airline for a beautiful statusline
Plug 'vim-airline/vim-airline'

"various colorschemes
Plug 'flazz/vim-colorschemes'

"Javascript autocompletion
Plug 'ternjs/tern_for_vim',{ 'do' : 'npm install' }

"Multiple cursors aka multiline editing
Plug 'terryma/vim-multiple-cursors'

"Ctrl-Shift-F
Plug 'dyng/ctrlsf.vim'

"fzf
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

call plug#end()


" autocmd BufWritePost *.js AsyncRun -post=checktime eslint --fix %
" autocmd BufWritePost *.jsx AsyncRun -post=checktime eslint --fix %
let g:ale_sign_error = '●' " Less aggressive than the default '>>'
let g:ale_sign_warning = '.'
let g:ale_lint_on_enter = 0 " Less distracting when opening a new file
let g:ale_fixers = { 'javascript': ['prettier'] }

let g:user_emmet_leader_key=','
let g:user_emmet_settings = {
			\  'javascript.jsx' : {
			\      'extends' : 'jsx',
			\  },
			\}

"Exclude some files from CtrlP
set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux
set wildignore+=*\\tmp\\*,*.swp,*.zip,*.exe  " Windows
set wildignore+=*/node_modules/*,node_modules     " NODEJS

"Ctrl-P{{{
let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git'
let g:ctrlp_by_filename = 1
let g:ctrlp_show_hidden = 1
"}}}

"NERDtree{{{
"
"
"Using C-n to toggle NERDtree
map <Leader>n :NERDTreeToggle<CR>
noremap gl :NERDTreeFind<CR>

"Always open file in current window even if unsaved
set hidden
"Show hidden files by default
let NERDTreeShowHidden=1

"Ignore files
"Ignore Vim swap files, node_modules dir and NERDTree default ignore
let NERDTreeIgnore=['\.sw.$','^node_modules[[dir]]$', '\~$']

"}}}
"Use babylone parser
let g:prettier#config#parser = 'babylon'

"Vim commentary

"Vim Multicursor mappings{{{
let g:multi_cursor_use_default_mapping=0


" Default mapping
let g:multi_cursor_start_word_key      = '<C-n>'
let g:multi_cursor_select_all_word_key = '<A-n>'
let g:multi_cursor_start_key           = 'g<C-n>'
let g:multi_cursor_select_all_key      = 'g<A-n>'
let g:multi_cursor_next_key            = '<C-n>'
let g:multi_cursor_prev_key            = '<C-p>'
let g:multi_cursor_skip_key            = '<C-x>'
let g:multi_cursor_quit_key            = '<Esc>'
"}}}

"CtlSF mappings
nnoremap <leader><S-f> :CtrlSF

"Snippets{{{
"
"Mappings
 " Trigger configuration. Do not use <tab> if you use
 " https://github.com/Valloric/YouCompleteMe.
 let g:UltiSnipsExpandTrigger="<leader>ux"
 let g:UltiSnipsListSnippets="<c-tab>"
 let g:UltiSnipsJumpForwardTrigger="<c-h>"
 let g:UltiSnipsJumpBackwardTrigger="<c-l>"
 let g:UltiSnipsSnippetsDir = "/home/qymspace/dotfiles/vim/.vim/plugged/ultisnips/UltiSnips"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

"Open UltiSnips edit function
 nmap <leader>ue :UltiSnipsEdit<cr>
"
"
"}}}


" Custom Functions {{{
function! <SID>ToggleNumber()
	if(&relativenumber == 1)
		set norelativenumber
		set number
	else
		set relativenumber
	endif
endfunc
"}}}

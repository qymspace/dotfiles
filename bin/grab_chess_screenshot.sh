#!/bin/bash

#screenshot selection and copy to clipboard
maim -s ~/Code/puppeteer-grab-chess-screenshot/image.png

#use puppeteer to visit the page
#arguments are color | destination | chapter
#eg: white | file | 1.14
if node ~/Code/puppeteer-grab-chess-screenshot/index.js $1 $2 $3; then
	#take selection from clipboard to primary selection for it to work with scidvspc docker
	xclip  -o -selection clip| xclip -i -selection primary
	notify-send "Putzing complete!"

else
	notify-send "An error occured while puztzing" "$(xclip -o -selection clip)"
fi

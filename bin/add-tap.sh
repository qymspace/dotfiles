#bin/bash
tunctl -t tap0
ifconfig tap0 10.200.200.1 netmask 255.255.255.252 up
iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
iptables -A FORWARD -i tap0 -j ACCEPT

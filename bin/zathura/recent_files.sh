#!/bin/sh

options='-columns 6 -width 100 -lines 15 -bw 2 -yoffset -2 -location 1'

selected=$(\
	        cat ~/.local/share/zathura/history | grep -Po '\[\K[^\]]*' \
		        | rofi -dmenu -i -markup-rows \
			        ${options} 
        )

# exit if nothing is selected
[[ -z $selected ]] && exit

zathura "$selected"
#Kill zathura instances that dont have arguments to them ***NOT WORKING CURRENTLY***
kill $(ps aux | grep -e 'zathura$' | awk '{print $2}')


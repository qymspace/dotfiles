#Directory to Store screenshots
notify-send "maim_chess"
directory="$HOME/Pictures/Chess/FCM"
mkdir -p $directory

#Depending on whether it was rightclick or left click we can know that it was white ot move or black to move

#Get coordinates using xdotool
eval $(xdotool getmouselocation --shell)
echo "Coordinates X: $X Y: $Y"



#Contant width 
width=400
height=400
#Pass Coordinates to maim
geometry="${width}x${height}+${X}+${Y}"

maim -u -k -g "$geometry" "$directory/$(date +%s)$1.png";
echo $geometry
play ~/dotfiles/media/shutter.ogg



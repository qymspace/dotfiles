#!/data/data/com.termux/files/usr/bin/bash

#Few things to note. If a file is not split, it just ends with .mp4.enc, If it is split, it ends with 6ab.. if its the last
# file. 

echo "Starting merger"

#When shared to termux:
# termux-toast "$1"
echo "$1"
# exit 0
# 1. Get the name of this file. '$1'
#I have symlinked /sdcard/WhatsApp/Media/WhatsApp Documents to ~/docs
inputFolder="/data/data/com.termux/files/home/docs"
outputFolder="/data/data/com.termux/files/home/docs"
ls $inputFolder

 filename=`echo "$1" | sed -e 's/6ab489bf\-[0-9]*.*$//g'`
 echo "filename is $filename"

# 2. Check if there is another file "similar " to this 

cat "$inputFolder/$filename*" > "$outputFolder/$filename.combined"


# 3. If yes, merge this file with that one and output to output folder as a combined file
# 4. If not then just copy this file to the to the output folder and call it combined
# 5. User termux api to open the combined file
termux-open  "$outputFolder/$filename.combined"





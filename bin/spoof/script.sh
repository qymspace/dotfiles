#!/bin/bash
while IFS= read -r line
do
    if grep -q $line /proc/net/arp 
    then
        printf "Found %s \n" "$line"
    else
        printf "Did not find %s \n" "$line"
        notify-send "Try : "+$line
    fi
done < "/home/qymspace/spoof/current.mac"

#update current.mac
awk '{print $4}' /proc/net/arp > ~/spoof/current.mac